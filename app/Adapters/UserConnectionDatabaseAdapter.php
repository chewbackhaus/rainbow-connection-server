<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/6/17
 * Time: 8:55 PM
 */

namespace App\Adapters;

use App\Adapters\Contracts\UserConnectionAdapterInterface;
use Illuminate\Database\ConnectionInterface;
use App\UserConnection;
use Illuminate\Support\Facades\Log;

class UserConnectionDatabaseAdapter implements UserConnectionAdapterInterface
{
    /**
     * @var ConnectionInterface
     */
    private $database;

    /**
     * //TODO - should be a property somewhere
     * @var string
     */
    const CONNECTIONS_DB_TABLE_ = 'connections';

    /**
     * @var string
     */
    const DB_COLUMN_ID_ = 'id';

    /**
     * @var string
     */
    const DB_COLUMN_USERID_INBOUND_ = 'userid_inbound';

    /**
     * @var string
     */
    const DB_COLUMN_USERID_OUTBOUND_ = 'userid_outbound';
    
    /**
     * UserDatabaseAdapter constructor.
     * @param ConnectionInterface $database
     */
    public function __construct(ConnectionInterface $database)
    {
        $this->database         = $database;
    }

    /**
     * @param int $outboundUserId
     * @param int $inboundUserId
     * @return UserConnection|null
     */
    public function addUserConnection($outboundUserId, $inboundUserId)
    {
        //TODO validation
        Log::debug('addUserConnection ', [$outboundUserId, $inboundUserId]);
        
        $userConnection = new UserConnection;
        $userConnection->userid_outbound = $outboundUserId;
        $userConnection->userid_inbound = $inboundUserId;
        $rc = $userConnection->save();
        if (!$rc) {
            Log::error('error adding user connection', [$outboundUserId, $inboundUserId]);
            return null;
        }
        
        return $userConnection;
    }

    /**
     * @param int $userId
     * @param int $sortId
     * @param int $limit
     * @return null|array
     */
    public function loadUserConnections($userId, $sortId = 1, $limit = 100)
    {
        /** NOT THE MOST PERFORMANT PAGING SYSTEM - 
         * Went with this to save time, but would get really slow with
         * large datasets
         */
        
        $records = $this->database->table(self::CONNECTIONS_DB_TABLE_)->
            where([
                [self::DB_COLUMN_USERID_INBOUND_, '=', $userId]
            ])->
            limit($limit)->
            offset($sortId)->
            orderBy(self::DB_COLUMN_ID_, 'asc')->
            get();

        if (!$records) {
            Log::error('UserConnectionDatabaseAdapter - getUserConnections - failed talking to DB');
            return null;
        }
        
        $userConnectionsArr = [];
        foreach ($records as $dbRecord) {
            $userConnectionsArr[$dbRecord->userid_outbound] = new UserConnection(get_object_vars($dbRecord));
        }

        return $userConnectionsArr;
    }

    /**
     * @param int $userOutboundId
     * @param int $userInboundId
     * @return UserConnection|null
     */
    public function loadUserConnection($userOutboundId, $userInboundId)
    {
        $records = $this->database->table(self::CONNECTIONS_DB_TABLE_)->
            where([
                [self::DB_COLUMN_USERID_INBOUND_, '=', $userInboundId],
                [self::DB_COLUMN_USERID_OUTBOUND_, '=', $userOutboundId]
            ])->
            get();

        if (is_null($records)) {
            Log::error('UserConnectionDatabaseAdapter - loadUserConnection - failed talking to DB');
            return null;
        }

        $recordCount = count($records);
        if (count($records) > 1) {
            Log::warn('UserConnectionDatabaseAdapter - loadUserConnection - multiple keys!!??');
        }

        if ($recordCount === 0) {
            Log::debug('UserConnectionDatabaseAdapter - loadUserConnection - no records found for connection');
            return null;
        }

        return new UserConnection(get_object_vars($records[0]));
    }

    /**
     * More performant batch load of connections
     *
     * @param array $userIds
     * @return array of arrays            matrix of UserConnection objects indexed by user_id
     *                                      inner array indexed by OTHER user's id in UserConnection
     */
    public function loadConnectionBatch(array $userIds)
    {
        //TODO need a max value on this

        $query = $this->database->table(self::CONNECTIONS_DB_TABLE_);

        foreach ($userIds as $userId) {
            $query->orWhere(self::DB_COLUMN_USERID_INBOUND_, '=', $userId);
        }

        $records = $query->get();
        if (!$records) {
            Log::error('UserConnectionDatabaseAdapter - loadConnectionBatch - failed talking to DB');
            return null;
        }

        $userConnectionsMatrixArr = [];
        foreach ($records as $dbRecord) {
            $userInboundId = $dbRecord->userid_inbound;

            if (!isset($userConnectionsMatrixArr[$userInboundId])) {
                $userConnectionsMatrixArr[$userInboundId] = [];
            }

            $userConnectionsMatrixArr[$userInboundId][$dbRecord->userid_outbound] = new UserConnection(get_object_vars($dbRecord));
        }

        return $userConnectionsMatrixArr;
    }

    public function removeUserConnection($outboundUserId, $inboudUserId)
    {
        //TODO
    }
}