<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/6/17
 * Time: 7:41 PM
 */

namespace App\Adapters;

use App\Adapters\Contracts\UserAdapterInterface;
use Illuminate\Database\ConnectionInterface;
use App\User;
use Illuminate\Support\Facades\Log;

class UserDatabaseAdapter implements UserAdapterInterface
{
    /**
     * @var ConnectionInterface
     */
    private $database;

    /**
     * //TODO - should be a property somewhere
     * @var string
     */
    const USER_DB_TABLE_ = 'users';

    /**
     * @var string
     */
    const DB_COLUMN_ID_ = 'id';

    /**
     * UserDatabaseAdapter constructor.
     * @param ConnectionInterface $database
     */
    public function __construct(ConnectionInterface $database)
    {
        $this->database         = $database;
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public function loadUserById($userId)
    {
        Log::debug('loadUserById - ', ['userId' => $userId]);

        $userFromDb = User::find($userId);
        if (!$userFromDb) {
            Log::debug('user not found ', $userId);
            return null;
        }

        return $userFromDb;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $favoriteColorHex
     * @return User|null
     */
    public function addNewUser($firstName, $lastName, $favoriteColorHex)
    {
        //TODO do some validation here

        $user = new User;
        $user->first_name = $firstName;
        $user->last_name = $lastName;
        $user->fav_color_hex = $favoriteColorHex;

        $rc = $user->save();
        if (!$rc) {
            Log::error('failed saving user ' . [$firstName, $lastName, $favoriteColorHex]);
            return null;
        }

        return $user;
    }

    /**
     * @param int $sortId
     * @param int $limit
     * @return array|null        of User objects indexed by user id
     */
    public function loadUsers($sortId, $limit = 25)
    {
        $records = $this->database->table(self::USER_DB_TABLE_)->
            where(self::DB_COLUMN_ID_, '>', $sortId)->
            limit($limit)->
            orderBy(self::DB_COLUMN_ID_, 'asc')->
            get();
        
        if (!$records) {
            Log::error('UserDatabaseAdapter - loadUsers - failed talking to DB');
            return null;
        }

        $usersArr = [];
        foreach ($records as $dbRecord) {
            $usersArr[$dbRecord->id] = new User(get_object_vars($dbRecord));
        }
        
        return $usersArr;
    }

    /**
     * More performant batch load of connections
     *
     * @param array $userIds
     * @return array             of User objects indexed by user->id
     */
    public function loadUserBatch(array $userIds)
    {
        //TODO need a max value on this

        $query = $this->database->table(self::USER_DB_TABLE_);

        foreach ($userIds as $userId) {
            $query->orWhere(self::DB_COLUMN_ID_, '=', $userId);
        }

        $records = $query->get();
        if (!$records) {
            Log::error('UserDatabaseAdapter - loadUserBatch - failed talking to DB');
            return null;
        }

        $userBatchArr = [];
        foreach ($records as $dbRecord) {
            $userId = $dbRecord->id;
            $userBatchArr[$userId] = new User(get_object_vars($dbRecord));
        }

        return $userBatchArr;
    }
}