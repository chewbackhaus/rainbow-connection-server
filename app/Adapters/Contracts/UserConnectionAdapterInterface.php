<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/6/17
 * Time: 7:37 PM
 */

namespace App\Adapters\Contracts;

use App\UserConnection;

interface UserConnectionAdapterInterface
{
    /**
     * @param int $outboundUserId
     * @param int $inboundUserId
     * @return UserConnection|null
     */
    public function addUserConnection($outboundUserId, $inboundUserId);

    /**
     * @param int $userId
     * @param int $sortId
     * @param int $limit
     * @return null|array
     */
    public function loadUserConnections($userId, $sortId = 1, $limit = 100);

    /**
     * @param int $userOutboundId
     * @param int $userInboundId
     * @return UserConnection|null
     */
    public function loadUserConnection($userOutboundId, $userInboundId);

    /**
     * More performant batch load of connections
     *
     * @param array $userIds
     * @return array            of UserConnection objects
     */
    public function loadConnectionBatch(array $userIds);

    //TODO comments
    public function removeUserConnection($outboundUserId, $inboudUserId);
}