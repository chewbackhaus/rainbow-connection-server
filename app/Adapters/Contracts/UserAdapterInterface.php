<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/6/17
 * Time: 7:34 PM
 */

namespace App\Adapters\Contracts;

use App\User;

interface UserAdapterInterface
{
    /**
     * @param int $userId
     * @return User|null
     */
    public function loadUserById($userId);

    /**
     * @param int $sortId
     * @param int $limit
     * @return array        of User objects
     */
    public function loadUsers($sortId, $limit);

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $favoriteColorHex
     * @return User|null
     */
    public function addNewUser($firstName, $lastName, $favoriteColorHex);

    /**
     * More performant batch load of users
     *
     * @param array $userIds
     * @return array             of User objects indexed by user->id
     */
    public function loadUserBatch(array $userIds);
}