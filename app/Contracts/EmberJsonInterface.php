<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/7/17
 * Time: 6:19 PM
 */

namespace App\Contracts;

interface EmberJsonInterface
{
    /**
     * @return \Illuminate\Http\JsonResponse of data ready to be json_encoded
     */
    public function getJson($type);
}