<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/7/17
 * Time: 11:35 AM
 */

namespace App\Utilities;

class ColorUtils
{
    public static $SUPPORTED_COLORS = [
        'FF0000',      //  => 'red',
        'FFFF00',      //  => 'yellow',
        '006600',      //  => 'green',
        '0000FF',      //  => 'blue'
        //TODO ADD MORE
    ];

    private static $COLOR_COUNT_;

    /**
     * Get a random color hexcode
     *
     * @return string
     */
    public static function GetRandomColor()
    {
        if (!self::$COLOR_COUNT_) {
            self::$COLOR_COUNT_ = count(self::$SUPPORTED_COLORS);
        }

        return self::$SUPPORTED_COLORS[rand(0, self::$COLOR_COUNT_ - 1)];
    }
}