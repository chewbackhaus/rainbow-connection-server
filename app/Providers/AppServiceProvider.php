<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $userAdapter = new \App\Adapters\UserDatabaseAdapter(\Illuminate\Support\Facades\DB::connection('mysql'));
        $this->app->instance('\App\Adapters\UserDatabaseAdapter', $userAdapter);

        $userService = new \App\Services\UserService($userAdapter);
        $this->app->instance('\App\Services\UserService', $userService);

        $userConnectionAdapter = new \App\Adapters\UserConnectionDatabaseAdapter(\Illuminate\Support\Facades\DB::connection('mysql'));
        $this->app->instance('\App\Adapters\UserConnectionDatabaseAdapter', $userConnectionAdapter);

        $userConnectionsService = new \App\Services\UserConnectionService($userConnectionAdapter);
        $this->app->instance('\App\Services\UserConnectionService', $userConnectionsService);
    }
}
