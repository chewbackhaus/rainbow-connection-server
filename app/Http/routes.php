<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Http\Controllers\Api\LoadUserByIdController;
use App\Http\Controllers\Api\LoadUserConnectionsController;
use App\Http\Controllers\Test\LoadTestDataController;
use App\Http\Controllers\Api\LoadUsersWithConnectionsController;

Route::get('/', function () {
    return view('welcome');
});

/** wire up json API endpoints */

Route::get('/api/users/{id?}/connections/', 'Api\LoadUserConnectionsController@render');
Route::get('/api/users/{id}/', 'Api\LoadUserByIdController@render');
Route::get('/api/users/', 'Api\LoadUsersWithConnectionsController@render');

/** Test Endpoints */

Route::post('/testdata', 'Test\LoadTestDataController@populateDBs');

