<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/7/17
 * Time: 10:17 AM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserConnectionService;
use App\Services\UserService;
use Illuminate\Support\Facades\Log;

class LoadUserByIdController extends Controller
{
    /**
     * @var UserService
     * @private
     */
    private $userService_;

    /**
     * @var UserConnectionService
     * @private
     */
    private $userConnectionService_;

    /**
     * @var string
     */
    const GET_PAGE_ID_ = 'pageId';

    /**
     * LoadUsersController constructor.
     * @param UserService $userService
     * @param UserConnectionService $userConnectionService
     */
    public function __construct(UserService $userService, UserConnectionService $userConnectionService)
    {
        $this->userService_ = $userService;
        $this->userConnectionService_ = $userConnectionService;
    }

    public function render(Request $request, $userId)
    {
        //todo validate user id
        $user       = $this->userService_->loadUser($userId);
        if (!$user) {
            //TODO handle exception case
            Log::error('LoadUsersController - render - no user from db', $userId);
            return 'Whoops!';
        }
        
        return response()->json([
            'data' => $user->getJson('user')
        ]);
    }
}