<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/7/17
 * Time: 10:17 AM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserConnectionService;
use App\Services\UserService;
use Illuminate\Support\Facades\Log;

class LoadUsersWithConnectionsController extends Controller
{
    /**
     * @var UserService
     * @private
     */
    private $userService_;

    /**
     * @var UserConnectionService
     * @private
     */
    private $userConnectionService_;

    /**
     * @var string
     */
    const GET_PAGE_ID_ = 'pageId';

    /**
     * LoadUsersController constructor.
     * @param UserService $userService
     * @param UserConnectionService $userConnectionService
     */
    public function __construct(UserService $userService, UserConnectionService $userConnectionService)
    {
        $this->userService_ = $userService;
        $this->userConnectionService_ = $userConnectionService;
    }

    public function render(Request $request)
    {
//        $pageId = $request->input(self::GET_PAGE_ID_);
//        if (!$pageId) {
//            //TODO validate pageId More
//            $pageId = 1;
//        }
//
//        if ($pageId  >= 1) {
//            //decrement in these cases since client is 1 based
//            $pageId--;
//        }

        //TODO
        $pageId = 0;
        $usersArr = $this->userService_->getUsersByPage($pageId);

        if (!$usersArr) {
            //TODO handle exception case
            Log::error('LoadUsersWithConnectionsControllers - render - no users from db');
            return 'Whoops!';
        }

        $userIdsToLoadArr = array_keys($usersArr);
        $userConnectionsMatrixArr = $this->userConnectionService_->getUserConnectionBatch($userIdsToLoadArr);
        if (!$userConnectionsMatrixArr) {
            //todo could be ok
            Log::error('LoadUsersWithConnectionsControllers - render - no connections from db');
            return 'Whoops!';
        }

        Log::debug('got user db connection count', [
            'count of users to get connections' => count($userConnectionsMatrixArr)
        ]);

        $userIdConnectionsToLoadArr = [];
        foreach ($userConnectionsMatrixArr as $userId => $connectionsArr) {

            //users might not be distinct so make sure we only load what we need to
            foreach ($connectionsArr as $userConnection) {
                $userIdFromConnection = $userConnection->userid_outbound;
                $userIdConnectionsToLoadArr[$userIdFromConnection] = $userIdFromConnection;
            }
        }

        Log::debug('got user db connection count', [
            'count of users actual user connections' => count($userIdConnectionsToLoadArr)
        ]);

        $usersFromConnectionsArr = $this->userService_->getUserBatch($userIdConnectionsToLoadArr);
        if (!$usersFromConnectionsArr) {
            Log::error('LoadUsersWithConnectionsControllers - render - user batch failed');
            return 'Whoops';
        }

        return response()->json([
            'data' => $this->createResponseObject($usersArr, $usersFromConnectionsArr, $userConnectionsMatrixArr)
        ]);
    }

    /**
     * Create response object.  This one is a little more complicated, so have an example
     *
    {
        'data' =>
            'selected_user_refs' => [
                '43',
                '26',
                '85'
    ],
        'user_objects' => [ {
            "type": "people",
            "id": "123",
            "attributes": {
            "first-name": "Jeff",
            "last-name": "Atwood"
            'connection_user_refs' => [
                '1',
                '2'
            ]
        },{
            "type": "people",
            "id": "123",
            "attributes": {
            "first-name": "Jeff",
            "last-name": "Atwood"
        } ]
    }

     * passed by value because the could be big arrays and don't want to copy. destroying arrays after getting data
     * to save memory
     *
     * @param array &$usersSelectedArr               indexed by userId --- users selected to show
     * @param array &$usersFromConnectionsArr        indexed by userId --- users that we need data for connections
     * @param array &$userConnectionsMatrixArr       indexed by the other users in the connection's userId ---
     * @return array
     */
    private function createResponseObject(array &$usersSelectedArr, array &$usersFromConnectionsArr, array &$userConnectionsMatrixArr)
    {
        //TODO - THis function is pretty ugly - could make this cleaner

        $responseJsonArr = [];
        $userConnectionRefArr = [];
        
        foreach ($userConnectionsMatrixArr as $userId => $connectionsArr) {
            $userConnectionRefArr[$userId] = array_keys($connectionsArr);
        }

        $responseJsonArr['type'] = 'all-user';
        $responseJsonArr['selected-user-refs'] = [];
        $responseJsonArr['user-objects'] = [];

        foreach ($usersSelectedArr as $user) {
            $responseJsonArr['selected-user-refs'][] = $user->id;

            if (isset($responseJsonArr['user-objects'][$user->id])) {
                continue;
            }

            $responseJsonArr['user-objects'][$user->id] = $user->getJson('user');
            if (isset($userConnectionRefArr[$user->id])) {
                $responseJsonArr['user-objects'][$user->id]['connection-user-refs'] = $userConnectionRefArr[$user->id];
            }

            unset($user);
        }

        foreach ($usersFromConnectionsArr as $user) {

            if (!isset($responseJsonArr['user-objects'][$user->id])) {
                $responseJsonArr['user-objects'][$user->id] = $user->getJson('user');
            }

            unset($user);
        }

        return $responseJsonArr;
    }
}