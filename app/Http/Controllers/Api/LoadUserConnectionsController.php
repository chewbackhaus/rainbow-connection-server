<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/7/17
 * Time: 10:17 AM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserConnectionService;
use App\Services\UserService;
use Illuminate\Support\Facades\Log;

class LoadUserConnectionsController extends Controller
{
    /**
     * @var UserService
     * @private
     */
    private $userService_;

    /**
     * @var UserConnectionService
     * @private
     */
    private $userConnectionService_;

    /**
     * @var string
     */
    const GET_PAGE_ID_ = 'pageId';

    /**
     * LoadUsersController constructor.
     * @param UserService $userService
     * @param UserConnectionService $userConnectionService
     */
    public function __construct(UserService $userService, UserConnectionService $userConnectionService)
    {
        $this->userService_ = $userService;
        $this->userConnectionService_ = $userConnectionService;
    }

    public function render(Request $request, $userId)
    {
        $pageId = $request->input(self::GET_PAGE_ID_);
        if (!$pageId) {
            //TODO validate pageId More
            $pageId = 1;
        }

        if ($pageId  >= 1) {
            //decrement in these cases since client is 1 based
            $pageId--;
        }

        //todo validate user id
        $user       = $this->userService_->loadUser($userId);
        if (!$user) {
            //TODO handle exception case
            Log::error('LoadUserConnectionsController - render - no user from db', $userId);
            return 'Whoops!';
        }

        $userConnections = $this->userConnectionService_->getUserConnectionsByPage($user, $pageId);
        if (!$userConnections) {
            Log::error('LoadUserConnectionsController - render - no user connections', [
                'userid ' => $user,
                'pageId' => $pageId
            ]);
            return 'Whoops!';
        }

        //TODO NOT IDEAL!!! - we're calling DB too many times here - this should be a mass load
        // BUT NO TIME! ;D

        $userJsonArr = [];
        foreach ($userConnections as $userConnection) {
            $userFromConnection = $this->userService_->loadUser($userConnection->userid_outbound);
            if (!$userFromConnection) {
                Log::warn('can not load user with id ', $userConnection->userid_outbound);
                continue;
            }

//            var_dump($userFromConnection->getJson());

            $userJsonArr[] = $userFromConnection->getJson('userconnection');

            //unset to clean memory since could load alot
            unset($userFromConnection);
            unset($userConnection);
        }

        return response()->json([
            'data' => $userJsonArr
        ]);
    }
}