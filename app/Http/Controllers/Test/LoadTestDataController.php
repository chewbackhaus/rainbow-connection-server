<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/7/17
 * Time: 11:04 AM
 */

namespace App\Http\Controllers\Test;

use App\Adapters\UserDatabaseAdapter;
use App\Utilities\ColorUtils;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Services\UserConnectionService;
use App\Services\UserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LoadTestDataController extends Controller
{
    /**
     * @var UserService
     * @private
     */
    private $userService_;

    /**
     * @var UserConnectionService
     * @private
     */
    private $userConnectionService_;

    /**
     * @var {RandomNameGenerator}
     * @private
     */
    private $nameGenerator_;

    /**
     * POST parameter constant
     * @var string
     */
    const POST_USER_COUNT = 'userCount';

    /**
     * max random connections per user
     * @int
     */
    const RANDOM_CONNECTIONS_COUNT = 50;

    /**
     * LoadUsersController constructor.
     * @param UserService $userService
     * @param UserConnectionService $userConnectionService
     */
    public function __construct(UserService $userService, UserConnectionService $userConnectionService)
    {
        $this->userService_ = $userService;
        $this->userConnectionService_ = $userConnectionService;
        $this->nameGenerator_ = \Nubs\RandomNameGenerator\All::create();
    }

    /**
     * Populate databases with test info
     *
     * @param Request $request
     * @return string
     */
    public function populateDBs(Request $request)
    {
        $userCount = $request->input('userCount');

        $this->cleanDatabases();
        return $this->generateRandomUsers($userCount);
        
    }

    /**
     * Generate Randome users with connections
     *
     * @param int $userCount
     * @return string
     */
    private function generateRandomUsers($userCount)
    {
        //Create users first
        $userCreatedCount = 0;
        $newUserIds = [];

        // create the number of users based on GET param
        for (; $userCreatedCount < $userCount; $userCreatedCount++) {
            $newUser = $this->generateRandomUser();
            $newUsers[] = $newUser;
        }

        // Add connections for users
        $connectionCount = 0;

        //get the count of users created by DB in case of failure.
        $userCreatedCount = count($newUsers);
        $userConnectionsCount = [];
        for ($i = 0; $i < $userCreatedCount; $i++) {
            // get the user id returned from the db
            $currentUserId = $newUsers[$i]->id;

            // number of random connections to make between 0 and 50
            $randConnectionsCount = rand(0, self::RANDOM_CONNECTIONS_COUNT);

            if (!isset($userConnectionsCount[$currentUserId])) {
                $userConnectionsCount[$currentUserId] = 0;
            }

            for ($j = 0; $j < $randConnectionsCount; $j++) {
                $randOtherUserIndex = rand(0, $userCreatedCount - 1);
                $otherUser = $newUsers[$randOtherUserIndex];

                if (!isset($userConnectionsCount[$otherUser->id])) {
                    $userConnectionsCount[$otherUser->id] = 0;
                }

                if ($userConnectionsCount[$otherUser->id] >= self::RANDOM_CONNECTIONS_COUNT) {
                    // other user has too many connections, go to next one
                    continue;
                }

                if ($userConnectionsCount[$currentUserId] >= self::RANDOM_CONNECTIONS_COUNT) {
                    //current user has hit limit, so let's break out of this loop and move to next user
                    break;
                }

                $this->userConnectionService_->addBidirectionalUserConnection($currentUserId, $otherUser->id);
                $connectionCount++;

                $userConnectionsCount[$currentUserId]++;
                $userConnectionsCount[$otherUser->id]++;

                unset($otherUser);
                unset($otherUserConnections);
            }

            unset($currentUserConnections);
        }

        return 'created ' . $userCreatedCount . ' users with ' . $connectionCount . ' connections';
    }

    /**
     * Build a new user with a random name and random favorite color
     *
     * @return \App\User|null
     */
    private function generateRandomUser()
    {
        $name = $this->nameGenerator_->getName();
        $favColor = ColorUtils::GetRandomColor();
        $parts = explode(' ', $name);
        return $this->userService_->addNewUser($parts[0], $parts[1], $favColor);
    }

    /**
     * Clean dbs for test endpoint
     */
    private function cleanDatabases()
    {
        //########## SUPER SCARY ##############//
        // TODO
        /** I'd make sure this code was never callable or deployed to prod, but for the exercise, I'm not worrying about it*/

        $migration = new \CreateRainbowConnectionsBackend();
        $migration->down();
        $migration->up();
    }
    
}