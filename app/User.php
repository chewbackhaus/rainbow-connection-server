<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\EmberJsonInterface;

class User extends Model implements EmberJsonInterface
{
//      WTF - laravel won't let me define these
//
//    /**
//     * @var int
//     */
//    public $id;
//
//    /**
//     * @var string
//     */
//    public $first_name;
//
//    /**
//     * @var string
//     */
//    public $last_name;
//
//    /**
//     * @var string
//     */
//    public $fav_color_hex;

    /**
     * The attributes that aren't mass assignable.
     *
     * allow setting all object properties
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * User constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @inheritdoc
     */
    public function getJson($type)
    {
        return [
            'type' => $type,
            'id' => $this->id,
            'attributes' => [
                'first-name' => $this->first_name,
                'last-name' => $this->last_name,
                'fav-color-hex' => $this->fav_color_hex
            ]
        ];
    }
}
