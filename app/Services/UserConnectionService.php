<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/6/17
 * Time: 7:24 PM
 */

namespace App\Services;

use App\Adapters\Contracts\UserConnectionAdapterInterface;
use App\User;
use Illuminate\Support\Facades\Log;

class UserConnectionService
{
    /**
     * @var UserConnectionAdapterInterface;
     */
    private $userConnectionsAdapter_;

    /**
     * UserService constructor.
     * @param UserConnectionAdapterInterface $userAdapter
     */
    public function __construct(UserConnectionAdapterInterface $userAdapter)
    {
        $this->userConnectionsAdapter_ = $userAdapter;
    }

    /**
     * @param User|int $userA
     * @param User|int $userB
     * @return bool
     */
    public function addBidirectionalUserConnection($userA, $userB)
    {
        $userAId = $userA;
        $userBId = $userB;

        if ($userA instanceof User) {
            $userAId = $userA->id;
        }

        if ($userB instanceof User) {
            $userBId = $userB->id;
        }

        //check if connection is already there.
        $userAOldInboundConnection = $this->userConnectionsAdapter_->loadUserConnection($userAId, $userBId);
        if (!$userAOldInboundConnection) {
            // no connection yet, let's add one

            $userAConnection = $this->userConnectionsAdapter_->addUserConnection($userAId, $userBId);
            if (!$userAConnection) {
                Log::error('Error creating outbound UserConnection for user A ids', [$userAId, $userBId]);
                return false;
            }
        }

        //check if connection is already there.
        $userBOldInboundConnection = $this->userConnectionsAdapter_->loadUserConnection($userBId, $userAId);
        if (!$userBOldInboundConnection) {
            // no connection yet, let's add one

            $userBConnection = $this->userConnectionsAdapter_->addUserConnection($userBId, $userAId);
            if (!$userBConnection) {
                Log::error('Error creating outbound UserConnection for user B ids', [$userBId, $userAId]);
                return false;
            }
        }

        return true;
    }

    /**
     * @param User $user
     * @param int $pageId
     * @param int $pageSize
     * @return array|null
     */
    public function getUserConnectionsByPage(User $user, $pageId = 0, $pageSize = 25)
    {
        $sortId = $pageId * $pageSize;
        Log::debug('UserConnectionService - getUserConnectionsByPage', [
            'sortId' => $sortId
        ]);
        $userConnections = $this->userConnectionsAdapter_->loadUserConnections($user->id, $sortId, $pageSize);
        
        if (!$userConnections) {
            Log::error('UserConnectionService - getUserConnectionsByPage - Could not retrieve connections');
            return null;
        }
        
        return $userConnections;
    }

    /**
     * More performant batch load of connections
     *
     * @param array $userIds
     * @return array of arrays            matrix of UserConnection objects indexed by user_id
     */
    public function getUserConnectionBatch(array $userIds)
    {
        $userConnectionsMatrixArr = $this->userConnectionsAdapter_->loadConnectionBatch($userIds);
        if (!$userConnectionsMatrixArr) {
            Log::error('UserConnectionService - getUserConnectionBatch - Could not retrieve connections');
            return null;
        }

        return $userConnectionsMatrixArr;
    }
}