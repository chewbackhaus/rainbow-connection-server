<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/6/17
 * Time: 7:24 PM
 */

namespace App\Services;

use App\Adapters\Contracts\UserAdapterInterface;
use App\User;
use Illuminate\Support\Facades\Log;

class UserService
{
    /**
     * @var UserAdapterInterface;
     * @private
     */
    private $userAdapter_;

    /**
     * UserService constructor.
     * @param UserAdapterInterface $userAdapter
     */
    public function __construct (UserAdapterInterface $userAdapter)
    {
        $this->userAdapter_ = $userAdapter;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $favoriteColorHex
     * @return User|null
     */
    public function addNewUser($firstName, $lastName, $favoriteColorHex)
    {
        //TODO validation

        return $this->userAdapter_->addNewUser($firstName, $lastName, $favoriteColorHex);
    }

    /**
     * @param int $pageId
     * @param int $pageSize
     * @return array|null
     */
    public function getUsersByPage($pageId = 0, $pageSize = 25)
    {
        //TODO validation

        $sortId = $pageId * $pageSize;
        $usersArr = $this->userAdapter_->loadUsers($sortId, $pageSize);
        
        if (!$usersArr) {
            Log::error('UserService - getUsersByPage - results from db empty');
            return null;
        }
        
        return $usersArr;
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public function loadUser($userId)
    {
        if (!$userId) {
            Log::error('UserService - loadUser - no user id provided');
            return null;
        }

        $user = $this->userAdapter_->loadUserById($userId);
        if (!$user) {
            Log::error('UserService - loadUser - no user from db');
            return null;
        }

        return $user;
    }

    /**
     * More performant batch load of users
     *
     * @param array $userIds
     * @return array            of User objects indexed by user->id
     */
    public function getUserBatch(array $userIds)
    {
        $userArr = $this->userAdapter_->loadUserBatch($userIds);
        if (!$userArr) {
            Log::error('UserService - getUserBatch - Could not retrieve batch users');
            return null;
        }

        return $userArr;
    }
}