<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\EmberJsonInterface;

class UserConnection extends Model implements EmberJsonInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'connections';
    
// LARAVEL won't let me set these;

//    public $id;
//
//    public $userid_inbound;
//
//    public $userid_outbound;

    /**
     * The attributes that aren't mass assignable.
     *
     * allow setting all object properties
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * UserConnection constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @inheritdoc
     */
    public function getJson($type)
    {
        return response()->json([
            'data' => [
                'type' => $type,
                'id' => $this->id,
                'attributes' => [
                    'userid-inbound' => $this->first_name,
                    'userid-outbound' => $this->last_name,
                ]
            ]
        ]);
    }
}
