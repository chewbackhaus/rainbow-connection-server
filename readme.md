# Prerequisites

Check out the client side project ```rainbow-connection-client``` where the bulk of my
notes are.

# Build

I didn't have time to package this one up as nice unfortunately, so let me know if you have issues.  I would have
normally made this much easier like the client.

### Get the code

* ```git clone git@bitbucket.org:chewbackhaus/rainbow-connection-server.git```
* ```cd rainbow-connection-server```
* ```git clone https://github.com/Laradock/laradock.git```
* ```cd laradock```
* ```cp env-example .env```
* update mysql property ```DB_HOST=mysql``` in .env file
* ```update the apache port in your laradock/.env file to APACHE_HOST_HTTP_PORT=8080```

### Create the Database

Before you build the mysql container, you need to update the `createdb.sql` file so the container will have access to the file

* `cd laradock/mysql/docker-entrypoint-initdb.d`
* `cp createdb.sql.example createdb.sql`
* Delete contents of `createdb.sql` and add the code below:

`CREATE DATABASE IF NOT EXISTS ``rainbow_connections`` COLLATE 'utf8_general_ci' ;
GRANT ALL ON ``rainbow_connections``.* TO 'default'@'%';

FLUSH PRIVILEGES ;
`

* Start your mysql container `docker-compose up mysql`
* Log in with `docker-compose exec mysql bash`
* run `mysql -uroot -proot < /docker-entrypoint-initdb.d/createdb.sql` to create db

### Finish start containers

Back on your terminal

* Start apache ```docker-compose up -d apache2```
* Log into worker ```docker-compose exec workspace bash```
  * On the worker ```php artisan key:generate```
  * On the worker ```php artisan migrate```

It should be up and running at this point on localhost:8080