<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use App\Adapters\UserConnectionDatabaseAdapter;
use App\Adapters\UserDatabaseAdapter;

include_once __DIR__ . '/TestUtils.php';

class UserConnectionAdapterTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserConnectionAdapter()
    {
        $db = DB::connection('mysql');
        $userConnectionDBAdapter = new UserConnectionDatabaseAdapter($db);
        $userDBAdapter = new UserDatabaseAdapter($db);

        $brianUser = $userDBAdapter->addNewUser('brian', 'b', 'ffffff');
        $this->assertEquals(1, $brianUser->id);
        $shivaUser = $userDBAdapter->addNewUser('shiva', 'b', '000000');
        $this->assertEquals(2, $shivaUser->id);

        $userConnection = $userConnectionDBAdapter->addUserConnection($shivaUser->id, $brianUser->id);

        $this->assertNotNull($userConnection->userid_outbound);
        $this->assertNotNull($userConnection->userid_inbound);
        $this->assertEquals($brianUser->id, $userConnection->userid_inbound);
        $this->assertEquals($shivaUser->id, $userConnection->userid_outbound);
    }

    /**
     * test isn't great since has some random elements, but NO TIME! ;D
     */
    public function testLoadUserConnectionBatch()
    {
        $db = DB::connection('mysql');
        $users = TestUtils::GenerateRandomUserConnections($db, 50);
        $userConnectionService = TestUtils::GetUserConnectionServiceInstance($db);
        $userService = TestUtils::GetUserServiceInstance($db);
        
        $userIdsArr = [];
        foreach($users as $user) {
            $userIdsArr[] = $user->id;
        }

        $userConnectionDBAdapter = new UserConnectionDatabaseAdapter($db);
        $userConnections = $userConnectionDBAdapter->loadConnectionBatch($userIdsArr);

        //just verify a few of these for now
        $userA = $userService->loadUser($userIdsArr[0]);
        $userAConnections = $userConnectionService->getUserConnectionsByPage($userA, 0, 500);

        $userB = $userService->loadUser($userIdsArr[1]);
        $userBConnections = $userConnectionService->getUserConnectionsByPage($userB, 0, 500);

        $userC = $userService->loadUser($userIdsArr[2]);
        $userCConnections = $userConnectionService->getUserConnectionsByPage($userC, 0, 500);

        $this->assertEquals(count($userAConnections), count($userConnections[$userA->id]));
        $this->assertEquals(count($userBConnections), count($userConnections[$userB->id]));
        $this->assertEquals(count($userCConnections), count($userConnections[$userC->id]));
    }
}
