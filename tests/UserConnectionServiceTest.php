<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\UserService;
use App\Services\UserConnectionService;
use App\Adapters\UserDatabaseAdapter;
use Illuminate\Support\Facades\DB;
use App\Adapters\UserConnectionDatabaseAdapter;

include_once __DIR__ . '/TestUtils.php';

class UserConnectionServiceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserConnectionService()
    {
        //TODO could mock

        $db = DB::connection('mysql');

        // user service
        $userDBAdapter = new UserDatabaseAdapter($db);
        $userService = new UserService($userDBAdapter);

        // user connection service
        $userConnectionDBAdapter = new UserConnectionDatabaseAdapter($db);
        $userConnectionService = new UserConnectionService($userConnectionDBAdapter);

        $userBrian = $userService->addNewUser('brian', 'b', 'ffffff');
        $this->assertEquals('brian', $userBrian->first_name);
        $this->assertNotNull($userBrian->id);

        $shivaUser = $userService->addNewUser('shiva', 'backhaus', 'ffffff');
        $this->assertEquals('shiva', $shivaUser->first_name);
        $this->assertNotNull($shivaUser->id);

        $rc = $userConnectionService->addBidirectionalUserConnection($userBrian, $shivaUser);
        $this->assertTrue($rc);
    }
    
    public function testLoadUserConnections()
    {
        $db = DB::connection('mysql');
        $userConnectionsService = TestUtils::GetUserServiceInstance($db);

        $usersArr = TestUtils::GetRandomUsers($db, 10);
        $userConnectionService = TestUtils::GetUserConnectionServiceInstance($db);

        $userA      = $usersArr[0];
        $userB      = $usersArr[1];
        $userC      = $usersArr[2];
        $userD      = $usersArr[3];
        $userE      = $usersArr[4];
        $userF      = $usersArr[5];
        $userG      = $usersArr[6];

        // create 6 mappings
        $userConnectionService->addBidirectionalUserConnection($userA, $userB);
        $userConnectionService->addBidirectionalUserConnection($userA, $userC);
        $userConnectionService->addBidirectionalUserConnection($userA, $userD);
        $userConnectionService->addBidirectionalUserConnection($userA, $userE);
        $userConnectionService->addBidirectionalUserConnection($userA, $userF);
        $userConnectionService->addBidirectionalUserConnection($userA, $userG);
        
        // load 3 pages of 2
        $page1Connections = $userConnectionService->getUserConnectionsByPage($userA, 0, 2);
        $pageSize = count($page1Connections);
        $this->assertEquals(2, $pageSize);
        $userConnectionAToB = $page1Connections[0];
        $userConnectionAToC = $page1Connections[1];
        $this->assertEquals($userA->id, $userConnectionAToB->userid_inbound);
        $this->assertEquals($userA->id, $userConnectionAToC->userid_inbound);
        $this->assertEquals($userB->id, $userConnectionAToB->userid_outbound);
        $this->assertEquals($userC->id, $userConnectionAToC->userid_outbound);

        $page2Connections = $userConnectionService->getUserConnectionsByPage($userA, 1, 2);
        $pageSize = count($page2Connections);
        $this->assertEquals(2, $pageSize);
        $userConnectionAToD = $page2Connections[0];
        $userConnectionAToE = $page2Connections[1];

        $this->assertEquals($userA->id, $userConnectionAToD->userid_inbound);
        $this->assertEquals($userA->id, $userConnectionAToE->userid_inbound);
        $this->assertEquals($userD->id, $userConnectionAToD->userid_outbound);
        $this->assertEquals($userE->id, $userConnectionAToE->userid_outbound);

        $page3Connections = $userConnectionService->getUserConnectionsByPage($userA, 2, 2);
        $pageSize = count($page3Connections);
        $this->assertEquals(2, $pageSize);
        $userConnectionAToF = $page3Connections[0];
        $userConnectionAToG = $page3Connections[1];

        $this->assertEquals($userA->id, $userConnectionAToF->userid_inbound);
        $this->assertEquals($userA->id, $userConnectionAToG->userid_inbound);
        $this->assertEquals($userF->id, $userConnectionAToF->userid_outbound);
        $this->assertEquals($userG->id, $userConnectionAToG->userid_outbound);

        //userA should only have one connection to any of the other users
        $userAConnections = $userConnectionService->getUserConnectionsByPage($userC);
        $connectionsASize = count($userAConnections);
        $this->assertEquals(1, $connectionsASize);
        $this->assertEquals($userC->id, $userAConnections[0]->userid_inbound);
        $this->assertEquals($userA->id, $userAConnections[0]->userid_outbound);
    }
}
