<?php
/**
 * Created by IntelliJ IDEA.
 * User: mybestday
 * Date: 7/6/17
 * Time: 11:02 PM
 */

use \App\Services\UserService;
use \App\Adapters\UserConnectionDatabaseAdapter;
use \App\Services\UserConnectionService;
use \App\Adapters\UserDatabaseAdapter;

class TestUtils
{
    /**
     * @param 
     * @param int $count
     * @return array|null
     */
    public static function GetRandomUsers($db, $count = 10)
    {
        $userService = self::GetUserServiceInstance($db);
        $generator = \Nubs\RandomNameGenerator\All::create();
        $usersArr = [];
        for ($i = 0; $i < $count; $i++) {
            $name = $generator->getName();
            $parts = explode(' ', $name);
            $usersArr[] = $userService->addNewUser($parts[0], $parts[1], 'ffffff');
        }

        return $usersArr;
    }

    /**
     * @param $db
     * @param int $countOfUser
     * @param int $maxConnectionsPerUser
     * @return array User           let's return the users since they're more useful in test
     */
    public static function GenerateRandomUserConnections($db, $countOfUser = 10, $maxConnectionsPerUser = 50)
    {
        $usersArr = self::GetRandomUsers($db, $countOfUser);
        $userConnectionService = self::GetUserConnectionServiceInstance($db);

        foreach ($usersArr as $user) {
            $randConnectionsPerUser = rand(0, $maxConnectionsPerUser);

            for ($i = 0; $i < $randConnectionsPerUser; $i++) {
                $randIndex = rand(0, $countOfUser - 1);
                $userConnectionService->addBidirectionalUserConnection($user, $usersArr[$randIndex]);
            }
        }

        return $usersArr;
    }

    public static function GetUserServiceInstance($db)
    {
        $userDBAdapter = new UserDatabaseAdapter($db);
        $userService = new UserService($userDBAdapter);

        return $userService;
    }

    /**
     * @param $db
     * @return UserConnectionService
     */
    public static function GetUserConnectionServiceInstance($db)
    {
        $userConnectionDBAdapter = new UserConnectionDatabaseAdapter($db);
        $userConnectionService = new UserConnectionService($userConnectionDBAdapter);

        return $userConnectionService;
    }
}