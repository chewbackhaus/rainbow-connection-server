<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\User;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateNewUser()
    {
        $user = new User;
        $user->first_name = 'Brian';
        $user->last_name = 'B';
        $user->fav_color_hex = 'FFFFFF';
        $user->save();

        $users = User::all();

        $this->assertEquals(1, $users->count());
        $this->assertEquals(1, $user->id);

        $userFromDb = $users[0];
        $this->assertEquals('Brian', $userFromDb->first_name);
        $this->assertEquals('B', $userFromDb->last_name);
        $this->assertEquals('FFFFFF', $userFromDb->fav_color_hex);
    }
}
