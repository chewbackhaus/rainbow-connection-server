<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\UserService;
use App\Adapters\UserDatabaseAdapter;
use Illuminate\Support\Facades\DB;

include_once __DIR__ . '/TestUtils.php';

class UserServiceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserService()
    {
        //TODO could mock

        $db = DB::connection('mysql');
        $userDBAdapter = new UserDatabaseAdapter($db);
        $userService = new UserService($userDBAdapter);

        $userBrian = $userService->addNewUser('brian', 'b', 'ffffff');
        $this->assertEquals('brian', $userBrian->first_name);
        $this->assertNotNull($userBrian->id);

        $shivaUser = $userService->addNewUser('shiva', 'backhaus', 'ffffff');
        $this->assertEquals('shiva', $shivaUser->first_name);
        $this->assertNotNull($shivaUser->id);
    }
    
    public function testLoadUsersByPage()
    {
        //populate users in DB
        $db = DB::connection('mysql');
        $users = TestUtils::GetRandomUsers($db, 50);
        $this->assertEquals(50, count($users));

        $firstHalf = array_slice($users, 0, 25);
        $secondHalf = array_slice($users, 25);

        $userService = TestUtils::GetUserServiceInstance($db);
        $usersFirstPageArr = $userService->getUsersByPage(0);
        $usersSecondPageArr = $userService->getUsersByPage(1);

        $size = count($firstHalf);
        for ($i = 0; $i < $size; $i++) {
            $userFromService = $usersFirstPageArr[$i];
            $userFromCache = $firstHalf[$i];

            $this->assertEquals($userFromCache->first_name, $userFromService->first_name);
            $this->assertEquals($userFromCache->last_name, $userFromService->last_name);
        }

        $size = count($secondHalf);
        for ($i = 0; $i < $size; $i++) {
            $userFromService = $usersSecondPageArr[$i];
            $userFromCache = $secondHalf[$i];

            $this->assertEquals($userFromCache->first_name, $userFromService->first_name);
            $this->assertEquals($userFromCache->last_name, $userFromService->last_name);
        }
    }
}
