<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Adapters\UserDatabaseAdapter;
use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;

include_once __DIR__ . '/TestUtils.php';

class UserAdapterTest extends TestCase
{
    use DatabaseMigrations;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserAdapter()
    {
        $db = DB::connection('mysql');
        $userDBAdapter = new UserDatabaseAdapter($db);
        $user = $userDBAdapter->addNewUser('brian', 'b', 'ffffff');
    }
    
    public function testGetUsersByPage()
    {
        $db = DB::connection('mysql');
        $userDBAdapter = new UserDatabaseAdapter($db);
        $users = TestUtils::GetRandomUsers($db, 100);
        
        $secondHalf = array_slice($users, 25, 50);
        
        $usersFromDbArr = $userDBAdapter->loadUsers(25, 50);
        $usersFromDbSize = count($usersFromDbArr);

        $this->assertEquals(50, $usersFromDbSize);

        for ($i = 0; $i < $usersFromDbSize; $i++) {
            $userFromDb = $usersFromDbArr[$i];
            $userFromCache = $secondHalf[$i];

            $this->assertEquals($userFromCache->first_name, $userFromDb->first_name);
        }
    }

    public function testGetUsersByBatch()
    {
        $db = DB::connection('mysql');
        $users = TestUtils::GetRandomUsers($db, 50);
        $userDBAdapter = new UserDatabaseAdapter($db);
        $userConnectionService = TestUtils::GetUserConnectionServiceInstance($db);
        $userService = TestUtils::GetUserServiceInstance($db);

        $userIdsArr = [];
        foreach($users as $user) {
            $userIdsArr[] = $user->id;
        }

        $userBatchArr = $userDBAdapter->loadUserBatch($userIdsArr);

        //just verify a few of these for now
        $userA = $users[0];
        $userB = $users[0];
        $userC = $users[0];

        $this->assertEquals($userA->first_name, $userBatchArr[$userA->id]->first_name);
        $this->assertEquals($userB->first_name, $userBatchArr[$userB->id]->first_name);
        $this->assertEquals($userC->first_name, $userBatchArr[$userC->id]->first_name);
    }
}
