<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\UserConnection;
use App\User;

class UserConnectionTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateUserConnection()
    {
        $this->assertTrue(true);
        
        $userBrian = new User;
        $userBrian->first_name = 'Brian';
        $userBrian->last_name = 'B';
        $userBrian->save();
        
        $userShiva = new User;
        $userShiva->first_name = 'Shiva';
        $userShiva->last_name = 'Backhaus';
        $userShiva->save();

        $this->assertEquals('Shiva', $userShiva->first_name);

        $newConnection = new UserConnection;
        $newConnection->userid_outbound = $userBrian->id;
        $newConnection->userid_inbound = $userShiva->id;
        $newConnection->save();

        $this->assertNotNull($newConnection->userid_inbound);
        $this->assertNotNull($newConnection->userid_outbound);
        $this->assertEquals($userBrian->id, $newConnection->userid_outbound);
        $this->assertEquals($userShiva->id, $newConnection->userid_inbound);
    }
}
