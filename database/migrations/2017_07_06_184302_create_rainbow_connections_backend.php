<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRainbowConnectionsBackend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('fav_color_hex', 6);
            $table->timestamps();
        });

        Schema::create('connections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('userid_inbound');
            $table->bigInteger('userid_outbound');
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['userid_inbound', 'userid_outbound']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('connections');
    }
}
